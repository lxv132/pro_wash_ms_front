// 注册一个全局自定义指令 `v-focus`
import Vue from 'vue'
//import $store from './vuex'
/**权限指令v-on v-bind v-text**/
/*id:表示指令的名称 v-for
* inserted: 当使用指令时；   
* */
Vue.directive('has',{
    inserted:function (el,binding) {
        let className = binding.value; //这个地方要注意了，看下面页面上的代码，对应取得相应的值
        if (el.parentNode && !Vue.prototype.$_has(className)) {
            el.parentNode.removeChild(el); //
        }
    }
})   
Vue.prototype.$_has = function (val) {//user:list
    //alert(val);
    let isShow = false;
    let btnPowerArr = localStorage.getItem("userButtonPermissions");
    // let btnPowerArr = $store.getters.getPermission;  === 先比较类型再比较值 '1'==1     ==  
    if(btnPowerArr===undefined || btnPowerArr===null){
        return false;
    }
    //["dept:query","dept:update","dept:add","user:add"].indexOf('user:add')
    // val 传递的值
    // ["user:add","user:modify","user:search"].indexOf('dept:add')
    if(btnPowerArr.indexOf(val) > -1){
        isShow = true;
    }
    return isShow;
}
