import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import './permissions'
import * as echarts from './echarts'

Vue.prototype.$echarts = echarts;

import BaiduMap from 'vue-baidu-map'

Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: "Kow5jQehMM6KzMiI6gLb7pVeRvATk0yd",
});


//设置路径的默认前缀 
axios.defaults.baseURL = "http://localhost:16868";
//把axios挂载到vue对象
Vue.prototype.$http = axios;
//使用axios拦截器，为所有请求加上token
axios.interceptors.request.use(function (config) {
  var token = localStorage.getItem("token");
  //比如是否需要设置 token
  if (token) {
    // alert(config.url);
    var url = config.url;
    // /dept/queryById 
    //alert(url); "123123123".indexOf("?")=-1 "123?123123".indexOf("?")=3
    if (url.indexOf("?") != -1) {
      config.url = url + "&token=" + token;
    } else {
      config.url = url + "?token=" + token;
    }
    //alert(config.url);
    //alert(JSON.stringify(config));
    //config.headers.token=sessionStorage.getItem("token");
  }
  return config;
})

// 配置如果是没有这个路径默认跳转到404
router.beforeEach((to, from, next) => {
  if (to.matched.length === 0) {
    // 如果未匹配到路由
    from.name ? next({ name: from.name }) : next("/404");
  } else {
    next(); // 如果匹配到正确跳转
  }
});

//让Vue引入使用ElementUI
Vue.use(ElementUI);

Vue.config.productionTip = false

//路由之前拦截处理
router.beforeEach((to, from, next) => {
  //获取请求路径
  const path = to.path;
  //放行路径
  if (path === "/Login") {
    //next 让程序继续运行
    return next();
  }
  //获取token值
  const token = localStorage.getItem("token");
  // token不空，放行
  if (token) {
    //alert(next);
    //alert(to);
    // alert(111);
    next()
  } else {
    //为空 到登录页面
    next({ path: '/Login' })
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
