import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

/* 先把VueRouter原型对象的push,先保存一份 */
let originPush=VueRouter.prototype.push;
let originReplace = VueRouter.prototype.replace
//重写push|replace
/* 第一个参数：告诉原来的push方法，往哪里跳转 */

//重写push
VueRouter.prototype.push=function(location,resolve,reject){
  if(resolve&&reject){
    //call||apply区别
    //相同点，都可以调用函数一次，都可以篡改函数的上下文一次
    //不同点：call与apply传递参数：call传递参数用逗号隔开，apply方法执行。传递数组
    //this还是用该上下文调用，也就是vuerouter的实例，后面跟参数指定跳的位置，以及成功失败的回调
    originPush.call(this,location,resolve,reject)
  }else{
    originPush.call(this,location,()=>{},()=>{})
  }
}


//重写replace
VueRouter.prototype.replace = function(location,resolve,reject){
  if(resolve&&reject){
    originReplace.call(this,location,resolve,reject)
  }else{
    originReplace.call(this,location,()=>{},()=>{})
  }
}

const routes = [
  // {
  //   path: "/Login",
  //   redirect: "Login",
  // },

  {
    path: "/",
    name: "Index",
    component: () => import("../views/limits of authority/Index.vue"),
    // component: (resolve) =>
    //   require([""], resolve),
    meta: { title: "自述文件" },
    children: [
      {
        path: "/",
        name: "Dashboard",
        component: () => import("../views/404/Dashboard.vue"),
        meta: { title: "系统首页" },
      },
      {
        path: "/Menu",
        name: "Menu",
        component: () => import("../views/limits of authority/Menu.vue"),
        meta: { title: "菜单管理" },
      },
      {
        path: "/User",
        name: "User",
        component: () => import("../views/limits of authority/User.vue"),
        meta: { title: "用户管理" },
      },
      {
        path: "/Role",
        name: "Role",
        component: () => import("../views/limits of authority/Role.vue"),
        meta: { title: "角色管理" },
      },
      {
        path: "/Address",
        name: "Address",
        component: () => import("../views/Address.vue"),
        meta: { title: "收获地址" },
      },
      {
        path: "/Balance",
        name: "Balance",
        component: () => import("../views/Balance.vue"),
        meta: { title: "哈哈地址" },
      },
      {
        path: "/Integral",
        name: "Integral",
        component: () => import("../views/Integral.vue"),
        meta: { title: "积分地址" },
      },
      {
        path: "/TutorialPicinfo",
        name: "TutorialPicinfo",
        component: () => import("../views/TutorialPicinfo.vue"),
        meta: { title: "图片地址" },
      },
      {
        path: "/TbStoreEvaluate",
        name: "TbStoreEvaluate",
        component: () => import("../views/TbStoreEvaluate.vue"),
        meta: { title: "评价地址" },
      },
      {
        path: "/TbStoreProduct",
        name: "TbStoreProduct",
        component: () => import("../views/TbStoreProduct.vue"),
        meta: { title: "商店地址" },
      },
      {
        path: "/TbStore",
        name: "TbStore",
        component: () => import("../views/Store.vue"),
        meta: { title: "门店管理" },
      },
      {
        path: "/TbStation",
        name: "TbStation",
        component: () => import("../views/TbStation.vue"),
        meta: { title: "xx壮地址" },
      },
      {
        path: "/TbImage",
        name: "TbImage",
        component: () => import("../views/TbImage.vue"),
        meta: { title: "门店轮播图" },
      },
      {
        path: "/CarWashRecord",
        name: "CarWashRecord",
        component: () => import("../views/CarWashRecord.vue"),
        meta: { title: "xx壮地址" },
      },
      {
        path: "/CheckoutInfo",
        name: "CheckoutInfo",
        component: () => import("../views/CheckoutInfo.vue"),
        meta: { title: "xx壮地址" },
      },
      {
        path: "/CreditInfo",
        name: "CreditInfo",
        component: () => import("../views/CreditInfo.vue"),
        meta: { title: "xx壮地址" },
      },
      {
        path: "/FeedbackInfo",
        name: "FeedbackInfo",
        component: () => import("../views/FeedbackInfo.vue"),
        meta: { title: "xx壮地址" },
      },
      {
        path: "/aboutUs",
        name: "aboutUs",
        component: () => import("../views/aboutUs.vue"),
        meta: { title: "信息管理" },
      },
      {
        path: "/tuser",
        name: "tuser",
        component: () => import("../views/tuser.vue"),
        meta: { title: "访客管理" },
      },
      {
        path: "/aboutUsImage",
        name: "aboutUsImage",
        component: () => import("../views/aboutUsImage.vue"),
        meta: { title: "图片管理" },
      },
    ],
  },
  {
    path: "/Login",
    name: "Login",
    component: () => import("../views/limits of authority/login/login.vue"),
  },
  {
    path: "/404",
    name: "404",
    component: () => import("../views/404/404.vue"),
    meta: { title: "404错误提示" },
  },
];

const router = new VueRouter({
  routes
})

export default router

//可以多次点击同一个路径，放置项目冗余

// 结束
